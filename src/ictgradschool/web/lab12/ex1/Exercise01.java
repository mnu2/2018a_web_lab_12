package ictgradschool.web.lab12.ex1;

import com.sun.org.apache.xml.internal.security.utils.resolver.implementations.ResolverLocalFilesystem;
import ictgradschool.web.lab12.Keyboard;

import java.io.Console;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.Key;
import java.sql.*;
import java.util.Properties;

public class Exercise01 {


    public static void main(String[] args) {
        /* The following verifies that your JDBC driver is functioning. You may base your solution on this code */
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");
            boolean good = true;
            while (good) {
                System.out.println("Please enter an article title:");

                try (PreparedStatement stmt = conn.prepareStatement("SELECT body FROM lab12_articles WHERE title LIKE ?")) {
                    stmt.setString(1, '%' + Keyboard.readInput() + '%');
                    try(ResultSet resultSet = stmt.executeQuery()){
                        if(resultSet.next()) {
                            resultSet.previous();
                            while (resultSet.next()) {
                                System.out.println(resultSet.getString(1));

                            }
                            good = false;
                        }
                        else {System.out.println("There are no matching article titles, please try another title.");}
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        } catch (SQLException e) {
        }
    }
}
