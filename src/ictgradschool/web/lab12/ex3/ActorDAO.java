package ictgradschool.web.lab12.ex3;

import com.sun.org.apache.bcel.internal.generic.Select;
import ictgradschool.web.lab12.Keyboard;
import ictgradschool.web.lab12.ex2.Article;

import java.io.FileInputStream;
import java.io.IOException;
import java.lang.annotation.ElementType;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ActorDAO {

    public ActorDAO(){}

    public String[] loadActorDets(String name) {

        Properties dbProps = new Properties();
        int actor_id = 0;
        String[] dets = new String[2];

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");

            try (PreparedStatement stmt = conn.prepareStatement("SELECT actor_id FROM pfilms_actor WHERE actor_fname LIKE ? OR actor_lname LIKE ?")) {
                stmt.setString(1, '%' + name + '%');
                stmt.setString(2, '%' + name + '%');
                try (ResultSet resultSet = stmt.executeQuery()) {


                    if (!resultSet.next()) {
                        System.out.println("Sorry, we couldn't find any actor by that name.");
                        return null;

                    } else {
                        actor_id = resultSet.getInt(1);
                        System.out.println(actor_id);
                    }

                }

            } catch (SQLException e) {
                e.printStackTrace();
            }
            try (PreparedStatement stmt = conn.prepareStatement("SELECT film.film_title, role.role_name FROM pfilms_film AS film, pfilms_participates_in AS p, pfilms_actor AS act, pfilms_role AS role WHERE film.film_id = p.film_id AND p.actor_id = act.actor_id AND p.role_id = role.role_id AND act.actor_id= ?")){
                stmt.setInt(1, actor_id);
                System.out.println("made it here");
                try (ResultSet resultSet1 = stmt.executeQuery()){
                    while(resultSet1.next()) {
                        String movies = resultSet1.getString(1);
                        String roles = resultSet1.getString(2);
                        System.out.println(movies + "(" + roles + ")");
                    }
                }
            }


        }
        catch (SQLException e1) {
            System.out.println(e1.getMessage());
            e1.printStackTrace();
        }
        return dets;
    }

    public static void main(String[] args) {
        ActorDAO ex = new ActorDAO();
        String[] array = ex.loadActorDets("Keanu");
    }
}
