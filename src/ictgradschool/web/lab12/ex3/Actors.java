package ictgradschool.web.lab12.ex3;

public class Actors{

    private int actor_id;
    private String actor_fname;
    private String actor_lname;
    private String movies;
    private String roles;


    public Actors(){};

    public String getActor_fname() {
        return actor_fname;
    }

    public String getActor_lname() {
        return actor_lname;
    }

    public int getActor_id() {
        return actor_id;
    }

    public String getMovies() {
        return movies;
    }

    public String getRoles() {
        return roles;
    }

    public void setActor_fname(String actor_fname) {
        this.actor_fname = actor_fname;
    }

    public void setActor_lname(String actor_lname) {
        this.actor_lname = actor_lname;
    }

    public void setActor_id(int actor_id) {
        this.actor_id = actor_id;
    }

    public void setMovies(String movies) {
        this.movies = movies;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }
}
