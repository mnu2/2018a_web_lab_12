package ictgradschool.web.lab12.ex2;

public class Article {
    private String title = null;
    private String content = null;

    public Article(){}

    public void setTitle(String s){
        title = s;
    };

    public void setContent(String s){
        content = s;
    }

    public String getTitle() {
        return title;
    }

    public String getContent(){
        return content;
    }
}
