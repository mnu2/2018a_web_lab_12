package ictgradschool.web.lab12.ex2;

import com.sun.org.apache.xerces.internal.dom.PSVIAttrNSImpl;
import ictgradschool.web.lab12.Keyboard;

import java.io.FileInputStream;
import java.io.IOException;
import java.security.AllPermission;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class ArticleDAO {

    public ArticleDAO() {

    }

    public List<Article> allArticles() {
        Properties dbProps = new Properties();

        try (FileInputStream fIn = new FileInputStream("mysql.properties")) {
            dbProps.load(fIn);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Set the database name to your database
        try (Connection conn = DriverManager.getConnection(dbProps.getProperty("url"), dbProps)) {
            System.out.println("Connection successful");
            boolean good = true;
            while (good) {
                System.out.println("Please enter an article title:");
                String input = Keyboard.readInput();
                try (PreparedStatement stmt = conn.prepareStatement("SELECT body, title FROM lab12_articles WHERE title LIKE ? AND title LIKE ?")) {
                    stmt.setString(1, '%' + input + '%');
                    stmt.setString(2, '%' + input + '%');
                    try (ResultSet resultSet = stmt.executeQuery()) {
                        List<Article> articles = new ArrayList<>();

                        if (!resultSet.next()) {
                            System.out.println("There are no matching article titles, please try another title.");

                        } else {
                            resultSet.previous();
                            good = false;
                            while (resultSet.next()) {
                                Article article = new Article();
                                article.setTitle(resultSet.getString(2));
                                article.setContent(resultSet.getString(1));
                                articles.add(article);
                            }
                            return articles;
                        }
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }


            }
        } catch (SQLException e) {
        }
        return null;
    }

    public Article getArticle(int articleId) {
        List<Article> ex = allArticles();
        return ex.get(articleId);
    }



}
